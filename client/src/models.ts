export interface IUser {
    id: number,
    email: string
}

export interface IGameType {
    id: number;
    name: string;
}

export interface IGameMember {
    id: number;
    game_id: number;
    user_id: number;
    order: number;
}

export interface IGame {
    id: number;
    type: number;
    members?: IGameMember[];
    game_cards?: ICard[];
    game_members?: IGameMember[];
    turn_owner?: number;
    status: GameStatus;
}

export interface ICard {
    id: number;
    game_id: number;
    name: string;
    img: string;
    owner_id?: number;
    status: CardStatus;
    attacking_card_id?: number;
}

export enum CardStatus {
    IN_DECK = 'IN_DECK',
    IN_HAND = 'IN_HAND',
    IN_ATTACK = 'IN_ATTACK',
    IN_DEFENCE = 'IN_DEFENCE',
    IN_DISCARD_PILE = 'IN_DISCARD_PILE'
}

export enum GameStatus {
    INIT = 'INIT',
    ATTACK = 'ATTACK',
    DEFENCE = 'DEFENCE',
    DONE = 'DONE'
}