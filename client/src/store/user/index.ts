import { AnyAction } from 'redux';

import { IUserReducer } from './types';
import { ACTIONS } from './actions';

const initialState: IUserReducer = {
    user: null
}

export default function userReducer(state: IUserReducer = initialState, action: AnyAction): IUserReducer {
    const { type, payload } = action;

    switch(type) {
        case ACTIONS.USER_SET_USER: {
            return {
                ...state,
                user: payload
            }
        }

        default: {
            return state;
        }
    }
}