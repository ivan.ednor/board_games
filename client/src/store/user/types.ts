import { AnyAction } from 'redux'

import { IUser } from '../../models';

export interface IUserReducer {
    user: null | IUser
}

// Action types

export interface ISetUserAction extends AnyAction {
    payload: IUser
}