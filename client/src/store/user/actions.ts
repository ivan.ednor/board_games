import { IUser } from '../../models';

import { ISetUserAction } from './types';

const USER_SET_USER: string = 'USER_SET_USER';

function setUser(user: IUser): ISetUserAction {
    return {
        type: USER_SET_USER,
        payload: user
    }
}

export const actions = {
    setUser
}

export const ACTIONS = {
    USER_SET_USER
}