import { IGamesReducer } from './games/types';
import { IUserReducer } from './user/types';

export interface IStore {
    user: IUserReducer;
    games: IGamesReducer;
}