import { IGame, IGameType } from '../../models';

import { ICreateGameAction, IGetExistingGames, IGetGameAction, IGetGameTypesAction, ISelectGameTypeAction } from './types';

const GET_EXISTING_GAMES = 'GET_EXISTING_GAMES'
const SELECT_GAME_TYPE = 'SELECT_GAME_TYPE'
const GET_GAME_TYPES = 'GET_GAME_TYPES'
const CREATE_GAME = 'CREATE_GAME'
const GET_GAME = 'GET_GAME'

function getGameTypes(gameTypes: IGameType[]): IGetGameTypesAction {
    return {
        type: GET_GAME_TYPES,
        payload: gameTypes
    }
}

function createGame(game: IGame): ICreateGameAction {
    return {
        type: CREATE_GAME,
        payload: game
    }
}

function selectGameType(gameTypeId: number): ISelectGameTypeAction {
    return {
        type: SELECT_GAME_TYPE,
        payload: gameTypeId
    }
}

function getGame(game: IGame): IGetGameAction {
    return {
        type: GET_GAME,
        payload: game
    }
}

function getExistingGames(games: IGame[]): IGetExistingGames {
    return {
        type: GET_EXISTING_GAMES,
        payload: games
    }
}

export const ACTIONS = {
    GET_EXISTING_GAMES,
    SELECT_GAME_TYPE,
    GET_GAME_TYPES,
    CREATE_GAME,
    GET_GAME
}

export const actions = {
    getGameTypes,
    createGame,
    selectGameType,
    getGame,
    getExistingGames
}