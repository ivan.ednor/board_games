import { AnyAction } from 'redux';

import { IGame, IGameType } from '../../models';

export interface IGamesReducer {
    gameTypes: IGameType[];
    game: IGame | null;
    gameType: number | null;
    games: IGame[];
}

export interface IGetGameTypesAction extends AnyAction {
    payload: IGameType[]
}

export interface ICreateGameAction extends AnyAction {
    payload: IGame;
}

export interface ISelectGameTypeAction extends AnyAction {
    payload: number;
}

export interface IGetGameAction extends AnyAction {
    payload: IGame;
}

export interface IGetExistingGames extends AnyAction {
    payload: IGame[];
}