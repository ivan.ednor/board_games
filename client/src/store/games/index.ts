import { AnyAction } from 'redux';

import { IGamesReducer } from './types';
import { ACTIONS } from './actions';

const initialState: IGamesReducer = {
    gameTypes: [],
    game: null,
    games: [],
    gameType: null
}

export default function gamesReducer(state: IGamesReducer = initialState, action: AnyAction): IGamesReducer {
    const { type, payload } = action;

    switch(type) {
        case ACTIONS.GET_GAME_TYPES: {
            return {
                ...state,
                gameTypes: payload
            }
        }
        case ACTIONS.GET_GAME:
        case ACTIONS.CREATE_GAME: {
            return {
                ...state,
                game: payload
            }
        }
        case ACTIONS.SELECT_GAME_TYPE: {
            return {
                ...state,
                gameType: payload
            }
        }
        case ACTIONS.GET_EXISTING_GAMES: {
            return {
                ...state,
                games: payload
            }
        }

        default: {
            return state;
        }
    }
}