import { createStore, combineReducers, AnyAction } from 'redux';

import { actions as gameActions, ACTIONS as GAME_ACTIONS } from './games/actions';
import { actions as userActions, ACTIONS as USER_ACTIONS } from './user/actions';
import gamesReducer from './games';
import userReducer from './user';

import { IStore } from './types';

const rootReducer = combineReducers({
    user: userReducer,
    games: gamesReducer
});

const store = createStore<IStore, AnyAction, {}, {}>(
    rootReducer,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

export const actions = {
    user: userActions,
    game: gameActions
}

export const ACTIONS = {
    USER: USER_ACTIONS,
    GAME: GAME_ACTIONS
}

export default store;