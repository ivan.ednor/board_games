import classNames from 'classnames';
import * as React from 'react';
import * as _ from 'lodash';

import { CardStatus, ICard, IUser } from '../../../models';

import './Card.scss';

export default class Card extends React.Component<IProps> {
    public render(): React.ReactNode {
        const { card: { img, status, name, owner_id }, index, user } = this.props;
        const isOwnCard = user.id === owner_id
        const isInDeck = status === CardStatus.IN_DECK
        const inTurnField = _.includes([CardStatus.IN_ATTACK, CardStatus.IN_DEFENCE], status)

        return (
            <div
                className={classNames(
                    'card', `card-${index}`
                )}>
                {(!inTurnField && (isInDeck || !isOwnCard))
                    ? <img src='/img/cards/red_back.png' alt={'card_back'} />
                    : <img src={img} alt={name} />
                }
            </div>
        )
    }
}

interface IProps {
    card: ICard;
    index: number;
    user: IUser;
}