import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import * as React from 'react';

import { gameDidMount, getGameId } from '../utils';
import { IStore } from '../../../store/types';
import effects from '../../../sideEffects';
import GameField from './GameField';
import Game from '../Game';

import { IConnectedState, IProps } from './types';

type MergedPropsType = IProps & { match: any }
class Durak extends Game<MergedPropsType> {
    protected gameTitle = 'Durak'

    protected onCreateGame = async () => {
        const { playersCount } = this.state

        await effects.games.durak.createGame(playersCount)
    }

    protected renderSelectedGame(): React.ReactNode {
        const { game, user } = this.props

        return game && user && (
            <GameField {...{ game, user }} />
        )
    }

    public componentDidMount(): void {
        gameDidMount(
            getGameId(this.props)
        )
    }
}

function mapStateToProps({ games: { game, games }, user: { user } }: IStore): IConnectedState {
    return {
        games,
        game,
        user
    }
}

export default connect(mapStateToProps)(
    withRouter(Durak as any)
)