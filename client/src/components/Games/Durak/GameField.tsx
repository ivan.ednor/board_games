import classNames from 'classnames';
import * as React from 'react';
import * as _ from 'lodash';

import { CardStatus, GameStatus, ICard, IGame, IUser } from '../../../models';
import sideEffects from '../../../sideEffects';
import Card from './Card';

import './GameField.scss'

export default class GameField extends React.Component<IProps, IState> {
    public readonly state: IState = {
        cardForDefence: null
    }

    public render(): React.ReactNode {
        return (
            <div className='game-field'>
                {this.renderOtherPlayers()}

                {this.renderGameDeck()}

                {this.renderTurnField()}

                {this.renderHand()}

                {this.renderTurnStatus()}
            </div>
        )
    }

    private renderOtherPlayers(): React.ReactNode {
        const { game, user } = this.props

        return (
            <div className='other-players'>
                {_.chain(game.game_cards)
                    .filter(({ owner_id, status }) => owner_id !== user.id && status === CardStatus.IN_HAND)
                    .groupBy(({ owner_id }) => owner_id)
                    .map((cardsByOwner, groupIndex) =>
                        <div
                            key={groupIndex}
                            className='other-hand'>
                            <span>{groupIndex}:</span>

                            <div className='d-flex'>
                                {_.map(cardsByOwner, (card, index) => (
                                    <div key={card.id} className='mr-2'>
                                        <Card key={card.id} {...{ card, index, user }} />
                                    </div>
                                ))}
                            </div>
                        </div>
                    ).value()}
            </div>
        )
    }

    private renderGameDeck(): React.ReactNode {
        const { game, user } = this.props

        return (
            <div className='game-deck'>
                {_.chain(game.game_cards)
                    .filter(({ status }) => status === CardStatus.IN_DECK)
                    .map((card, index) => (
                        <Card key={card.id} {...{ card, index, user }} />
                    )).value()}
            </div>
        )
    }

    private renderTurnField(): React.ReactNode {
        const { game, user } = this.props
        const canDefend = this.isCanDefend()

        return (
            <div className='turn-field'>
                {_.chain(game.game_cards)
                    .filter(({ status }) =>
                        status === CardStatus.IN_ATTACK
                    ).map((card, index) => {
                        const cardForDefence = _.find(
                            game.game_cards,
                            ({ status, attacking_card_id }) =>
                                status === CardStatus.IN_DEFENCE && attacking_card_id === card.id
                        )
                        return (
                            <div
                                className={classNames(
                                    'mr-2', { pointer: canDefend }
                                )}
                                onClick={this.onTurnFieldCardClick(card)}
                                key={card.id}>
                                {cardForDefence?
                                    <div className='game-deck'>
                                        <Card {...{ card, index: 0, user }} />
                                        <Card {...{ card: cardForDefence, index: 50, user }} />
                                    </div>
                                    : <Card {...{ card, index, user }} />}
                            </div>
                        )
                    }).value()}
            </div>
        )
    }

    private renderHand(): React.ReactNode {
        const { game, user } = this.props
        const { cardForDefence } = this.state

        return (
            <div className='hand'>
                {_.chain(game.game_cards)
                    .filter(({ owner_id, status }) => owner_id === user.id && status === CardStatus.IN_HAND)
                    .map((card, index) => (
                        <div
                            key={card.id}
                            className={classNames(
                                'mr-2',
                                { pointer: this.isYourTurn(), active: _.get(cardForDefence, ['id']) === card.id }
                            )}
                            onClick={this.onHandCardClick(card)}>
                            <Card {...{ card, index, user }} />
                        </div>
                    ))
                    .value()}
            </div>
        )
    }

    private renderTurnStatus(): React.ReactNode {
        const { user, game } = this.props

        return (
            <div className='turn-status'>
                {(() => {
                    switch(game.turn_owner) {
                        case null: {
                            return 'Waiting for other players...'
                        }
                        case user.id: {
                            return 'Your turn'
                        }
                        default: {
                            return 'Waiting for other players turn...'
                        }
                    }
                })()}
            </div>
        )
    }

    private onHandCardClick = (card: ICard) => async () => {
        if(!this.isYourTurn()) {
            return null
        }

        const { game } = this.props

        switch(game.status) {
            case GameStatus.ATTACK: {
                await sideEffects.games.durak.attack(game, card)
                break;
            }
            case GameStatus.DEFENCE: {
                this.setStateValue('cardForDefence')(card)
                break;
            }
        }
    }

    private onTurnFieldCardClick = (card: ICard) => async () => {
        if(!this.isCanDefend()) {
            return null
        }

        const { game } = this.props
        const { cardForDefence } = this.state

        sideEffects.games.durak.defence(game, cardForDefence!, card)
    }

    private isYourTurn = () => {
        const { game, user } = this.props

        return game.turn_owner === user.id
    }

    private isCanDefend = () => {
        const { game } = this.props
        const { cardForDefence } = this.state

        return game.status === GameStatus.DEFENCE && cardForDefence
    }

    private setStateValue = (key: string) => (value: any) => {
        this.setState({
            [key]: value
        } as any)
    }
}

interface IProps {
    game: IGame;
    user: IUser;
}

interface IState {
    cardForDefence: ICard | null;
}