import { IGame, IUser } from '../../../models';

export interface IConnectedState {
    game: IGame | null;
    games: IGame[];
    user: IUser | null;
}

export interface IProps extends IConnectedState {
    
}