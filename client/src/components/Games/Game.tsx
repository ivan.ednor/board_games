import { Button } from 'reactstrap';
import * as React from 'react';
import * as _ from 'lodash';

import sideEffects from '../../sideEffects';
import { IGame } from '../../models';
import { history } from '../App';

import { getGameId, getGameTypeParam } from './utils';

export default class Game<P> extends React.Component<P & IProps, IState> {
    protected gameTitle = 'Game'

    protected onCreateGame() {
        throw new Error('Implement onCreateGame method!')
    }

    protected renderSelectedGame(): React.ReactNode {
        throw new Error('Implement onCreateGame render method!')
    }

    public componentDidMount(): void {
        throw new Error('Override componentDidMount method!')
    }

    protected onSelectGame = (game: IGame) => async () => {
        await sideEffects.games.durak.selectExistingGame(game)
        history.push(
            `/${getGameTypeParam()}/${game.id}`
        )
    }

    public readonly state: IState = {
        playersCount: 2
    }

    public render(): React.ReactNode {
        const gameId = getGameId(this.props)

        return (
            <div className='durak'>
                <h4>{this.gameTitle}</h4>

                {gameId
                    ? this.renderSelectedGame()
                    : (
                        <>
                            {this.renderCreateGame()}

                            {this.renderExistingGames()}
                        </>
                    )}
            </div>
        );
    }

    protected renderCreateGame(): React.ReactNode {
        const { playersCount } = this.state

        return (
            <div className='d-flex align-items-center mt-4'>
                <Button className='mr-4' color='success' onClick={this.onCreateGame}>
                    Create Game
                </Button>

                <span className='mr-2'>Players count: </span>
                <select onChange={this.setSelectValue('playersCount')} value={playersCount}>
                    {_.chain(5)
                        .range()
                        .reject((num) => num < 2)
                        .map((num) => (
                            <option key={num} value={num}>
                                {num}
                            </option>
                        ))
                        .value()}
                </select>
            </div>
        )
    }

    protected renderExistingGames(): React.ReactNode {
        const { games } = this.props

        return !_.isEmpty(games) && (
            <>
                <h4 className='mt-4'>Or Choose Existing Game:</h4>

                <ul>
                    {_.map(games, (game) => (
                        <li key={game.id}>
                            <span className='link' onClick={this.onSelectGame(game)}>
                                Game #{game.id}
                            </span>
                        </li>
                    ))}
                </ul>
            </>
        )
    }

    private setSelectValue = (key: string) => (event: React.ChangeEvent<HTMLSelectElement>) => {
        this.setState({
            [key]: _.toNumber(event.target.value)
        } as any)
    }
}

interface IProps {
    match: any,
    games: IGame[]
}

interface IState {
    playersCount: number
}