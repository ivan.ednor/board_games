import * as _ from 'lodash';

import store, { actions } from '../../store';
import effects from '../../sideEffects';
import { history } from '../App';

export function getGameId(props: { match: { params: any }, [key: string]: any }): number | null {
    const gameIdParam = _.get(props.match.params, ['gameId'])

    return gameIdParam
        ? _.toNumber(gameIdParam)
        : null
}

export function getGameTypeParam(): string {
    return _.chain(history.location.pathname)
        .split('/')
        .get(1)
        .value()
}

export function getGameTypeName(): string {
    return _.startCase(
        getGameTypeParam()
    )
}

export async function gameDidMount(gameId: number | null): Promise<void> {
    const { dispatch, getState } = store
    const { games: { gameType: gameTypeFromState, gameTypes: gameTypesFromState } } = getState()

    let gameType = gameTypeFromState
    if(_.isNull(gameType)) {
        const gameTypeName = getGameTypeName();
        const gameTypes = _.isEmpty(gameTypesFromState)
            ? await effects.games.getGameTypes()
            : gameTypesFromState
        const gameTypeId = _.chain(gameTypes)
            .find(({ name }) =>  name === gameTypeName)
            .get(['id'])
            .value()

        gameType = gameTypeId

        dispatch(
            actions.game.selectGameType(gameType)
        )
    }

    effects.games.getExistingGames(gameType)

    if(gameId) {
        effects.games.getGame(_.toNumber(gameId))
    }
}