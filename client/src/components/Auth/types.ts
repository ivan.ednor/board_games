import { IUser } from '../../models';

export interface ISignUpFormProps {
    email: string,
    password?: string
}

export interface IProps {
    user: IUser
}