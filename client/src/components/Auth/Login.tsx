import { withFormik } from 'formik';
import validator from 'validator';

import effects from '../../sideEffects';
import { SignUp } from './SignUp';

class Login extends SignUp<any> {
    protected submitBtnText: string = 'Login'
}

export default withFormik({
    handleSubmit: effects.user.login,
    validate: ({ email }) => {
        const errors: any = {};

        if(!email) {
            errors.email = 'Required';
        } else if(!validator.isEmail(email)) {
            errors.email = 'Invalid email';
        }

        return errors;
    }
})(Login)