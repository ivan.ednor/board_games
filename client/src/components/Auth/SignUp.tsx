import { Input, Form, Button } from 'reactstrap';
import { withFormik, FormikProps } from 'formik';
import validator from 'validator';
import * as _ from 'lodash';
import React from 'react';

import API from '../../api';
import effects from '../../sideEffects';
import { history } from '../App';

import { IProps, ISignUpFormProps } from './types';

export class SignUp<T> extends React.Component<T & FormikProps<ISignUpFormProps> & IProps> {
    protected submitBtnText: string = 'Sign Up';

    public componentDidMount(): void {
        const { user } = this.props

        if(user) {
            history.push('/')
        }
    }

    public render(): React.ReactNode {
        const {
            errors: { email: emailError, password: passwordError },
            touched: { email: emailTouched, password: passwordTouched },
            handleChange, handleBlur, handleSubmit
        } = this.props;

        return (
            <Form>
                <Input
                    type='email' id='email' placeholder='Email'
                    onChange={handleChange} onBlur={handleBlur} />
                {emailTouched && !!emailError && (
                    <span className='text-danger'>
                        {emailError}
                    </span>
                )}

                <Input className='mt-4'
                    type='password' id='password' placeholder='Password'
                    onChange={handleChange} onBlur={handleBlur} />
                {passwordTouched && !!passwordError && (
                    <span className='text-danger'>
                        {passwordError}
                    </span>
                )}

                <div className='d-flex justify-content-end mt-4'>
                    <Button color='success' onClick={handleSubmit}>
                        {this.submitBtnText}
                    </Button>
                </div>
            </Form>
        );
    }
}

export default withFormik({
    handleSubmit: effects.user.signUp,
    validate: _.debounce(async (values) => {
        const { email, password } = values
        const errors: any = {};

        if(!email) {
            errors.email = 'Required';
        } else if(!validator.isEmail(email)) {
            errors.email = 'Invalid email';
        } else {
            const checkRes = await API.auth.checkEmailAvailability(values)

            if(checkRes && !checkRes.success) {
                errors.email = checkRes.error;
            }
        }

        if(!password) {
            errors.password = 'Required';
        }

        return errors;
    }, 250)
})(SignUp)