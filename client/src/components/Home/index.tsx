import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import * as _ from 'lodash';
import React from 'react';

import { IStore } from '../../store/types';
import { actions } from '../../store';
import { history } from '../App';

import { IConnectedDispatch, IConnectedState, IProps } from './types';

class Home extends React.Component<IProps> {
    public render(): React.ReactNode {
        const { gameTypes } = this.props

        return (
            <div className='d-flex flex-column'>
                <h4 className='text-center'>Choose a game:</h4>

                <ul>
                    {_.map(gameTypes, ({ id, name }) => (
                        <li key={id}>
                            <span className='link' onClick={this.onSelectGame(id, name)}>
                                {name}
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }

    private onSelectGame = (gameTypeId: number, gameTypeName: string) => async () => {
        const { selectGameType } = this.props

        await selectGameType!(gameTypeId)

        history.push(`/${_.lowerCase(gameTypeName)}`)
    }
}

function mapStateToProps({ games: { gameTypes } }: IStore): IConnectedState {
    return {
        gameTypes
    }
}

function mapDispatchToProps(dispatch: Dispatch): IConnectedDispatch {
    return {
        selectGameType: (gameType) => dispatch(actions.game.selectGameType(gameType))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)