import { IGameType } from '../../models';

export interface IConnectedState {
    gameTypes?: IGameType[];
}

export interface IConnectedDispatch {
    selectGameType?: (gameType: number) => void
}

export interface IProps extends IConnectedState , IConnectedDispatch{

}