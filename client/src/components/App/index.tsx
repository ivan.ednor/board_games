import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import React from 'react';

import effects from '../../sideEffects';
import { IStore } from '../../store/types';
import SignUp from '../Auth/SignUp';
import Durak from '../Games/Durak';
import Login from '../Auth/Login';
import Header from './Header';
import Home from '../Home';

import { IConnectedState, IProps } from './types';
import './index.scss';

export const history = createBrowserHistory();

class App extends React.Component<IProps> {
    private gameComponentsMap: any = {
        Durak
    }

    public componentDidMount(): void {
        effects.user.getMe();
        effects.games.getGameTypes();
    }

    public render(): React.ReactNode {
        const { gameTypes, user } = this.props

        return !_.isEmpty(gameTypes) && (
            <Router history={history}>
                <Header />
                <div className='app-content'>
                    <Switch>
                        <Route path='/' exact>
                            <Home />
                        </Route>
                        <Route path='/sign-up' exact>
                            <SignUp {...{ user }} />
                        </Route>
                        <Route path='/login' exact>
                            <Login {...{ user }} />
                        </Route>

                        {_.map(gameTypes, ({ id, name }) => {
                            const Game = this.gameComponentsMap[name];
                            const pathBase = `/${_.lowerCase(name)}`

                            return (
                                <React.Fragment key={id}>
                                    <Route path={pathBase} exact>
                                        <Game />
                                    </Route>
                                    <Route path={`${pathBase}/:gameId`} exact>
                                        <Game />
                                    </Route>
                                </React.Fragment>
                            )
                        })}
                        <Route path='/durak' exact>
                            <Durak />
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}

function mapStateToProps({ games: { gameTypes }, user: { user } }: IStore): IConnectedState {
    return {
        gameTypes,
        user
    }
}

export default connect(mapStateToProps)(App);