import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import React from 'react';

import { IStore } from '../../store/types';
import { IUser } from '../../models';

import './Header.scss';

class Header extends React.Component<IConnectedState> {
    public render(): React.ReactNode {
        const { user } = this.props

        return (
            <div className='header'>
                <Link to='/'>Home</Link>

                <div className='auth d-flex'>
                    {user
                        ? user.email
                        : (
                            <>
                                <Link to='/login'>Login</Link>
                                <Link to='/sign-up'>Sign Up</Link>
                            </>
                        )}
                </div>
            </div>
        );
    }
}

function mapStateToProps({ user: { user } }: IStore): IConnectedState {
    return {
        user
    }
}

export default connect(mapStateToProps)(Header)

interface IConnectedState {
    user: IUser | null;
}