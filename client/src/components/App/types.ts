import { IGameType, IUser } from '../../models';

export interface IConnectedState {
    gameTypes: IGameType[];
    user: IUser | null;
}

export interface IProps extends IConnectedState {

}