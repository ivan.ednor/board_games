// @ts-ignore
declare module 'snackbar' {
    export const show: (message: string) => void
}