import { IServerResponse } from '../types';
import { IUser } from '../../models';

export interface IGetMeResponse extends IServerResponse {
    user: IUser
}