import * as _ from 'lodash'
import axios from 'axios';

import { ISignUpFormProps } from '../../components/Auth/types';

import { IServerResponse } from '../types';
import { IGetMeResponse } from './types';

export async function signUp(values: ISignUpFormProps): Promise<IServerResponse | void> {
    const { data } = await axios.post('/api/auth/sign-up', values);

    return data;
}

export async function checkEmailAvailability(values: ISignUpFormProps): Promise<IServerResponse | void> {
    const { data } = await axios.post('/api/auth/check-email-availability', _.omit(values, ['password']));

    return data;
}

export async function login(values: ISignUpFormProps): Promise<IServerResponse | void> {
    // Unpretty code to handle 401 Unauthorized error
    try {
        return await new Promise(async (resolve, reject) => {
            const axiosInstance = axios.create();

            axiosInstance.interceptors.response.use(
                (response) => response,
                (error) => {
                    if (error.response.status === 401) {
                        reject({
                            success: false,
                            error: 'Unauthorized'
                        });
                    }
                }
            );

            try {
                const { data } = await axiosInstance.post('/api/auth/login', values);

                resolve(data);
            } catch(err) {
                console.log('login error - ', err)
            }
        });
    } catch(err) {
        return err
    }
}

export async function getMe(): Promise<void | IGetMeResponse> {
    const { data } = await axios.get('/api/auth/get-me');

    return data;
}