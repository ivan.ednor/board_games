import axios from 'axios'

import { IGame, IGameType } from '../../models'
import { IServerResponse } from '../types'

export async function getGameTypes(): Promise<(IServerResponse & { items: IGameType[] }) | void> {
    const { data } = await axios.get('/api/games/types')

    return data
}

export async function getGame(gameId: number): Promise<IServerResponse & { game: IGame } | void> {
    const { data } = await axios.get(`/api/games/${gameId}`)

    return data
}

export async function getExistingGames(gameType: number): Promise<(IServerResponse & { items: IGame[] }) | void> {
    const { data } = await axios.get(`/api/games/types/${gameType}`)

    return data
}