import axios from 'axios';

import { IServerResponse } from '../types';
import { IGame } from '../../models';

export async function createGame(playersCount: number): Promise<IGameResponse | void> {
    const { data } = await axios.post('/api/games/durak', { playersCount });

    return data;
}

export async function selectExistingGame(gameId: number): Promise<IGameResponse | void> {
    const { data } = await axios.put(`/api/games/durak/${gameId}`)

    return data
}

export async function attack(gameId: number, cardId: number): Promise<IGameResponse | void> {
    const { data } = await axios.post(`/api/games/durak/${gameId}/attack/${cardId}`)

    return data
}

export async function defence(gameId: number, cardForDefenceId: number, cardToAttackId: number): Promise<IGameResponse | void> {
    const { data } = await axios.post(`/api/games/durak/${gameId}/defence/${cardForDefenceId}/attack/${cardToAttackId}`)

    return data
}

interface IGameResponse extends IServerResponse {
    game: IGame;
}