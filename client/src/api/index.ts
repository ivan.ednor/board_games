import * as auth from './auth';
import * as games from './games';
import * as durak from './games/durak';

export default {
    auth,
    games: {
        ...games,
        durak
    }
}