export interface IServerResponse {
    success: boolean,
    error?: string,
    [key: string]: any
}