import socketIO from 'socket.io-client';

import { IGame } from './models';
import store, { actions } from './store';

const { dispatch, getState } = store

export let socket: any

export function init(): void {
    socket = socketIO(
        'http://localhost:1222',
        {
            transports: ['websocket'],
            reconnection: false
        }
    )

    socket.on('game', (fetchedGame: IGame) => {
        const { games: { game } } = getState()

        if(game && fetchedGame && fetchedGame.id === game.id) {
            dispatch(actions.game.getGame(fetchedGame))
        }
    })
    // @ts-ignore
    socket.on('disconnect', (msg) => {
        if(msg === 'io client disconnect') {
            init()
        }
    })
}