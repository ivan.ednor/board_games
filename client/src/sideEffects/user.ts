import snackbar from 'snackbar';
import * as _ from 'lodash';

import { ISignUpFormProps } from '../components/Auth/types';
import { history } from '../components/App';
import store, { actions } from '../store';
import { socket } from '../socket';
import API from '../api';

const { dispatch } = store;

export async function getMe(): Promise<void> {
    const getMeRes = await API.auth.getMe()

    if(getMeRes && getMeRes.success) {
        dispatch(actions.user.setUser(getMeRes.user))
    }
}

export async function login(values: ISignUpFormProps): Promise<void> {
    const res = await API.auth.login(values);

    if(res && res.success) {
        getMe()

        history.push('/')

        socket.disconnect(true)
    } else {
        snackbar.show(_.get(res, 'error', res))
    }
}

export async function signUp(values: ISignUpFormProps): Promise<void> {
    const res = await API.auth.signUp(values)

    if(res && res.success) {
        history.push('/login')
    }
}