import snackbar from 'snackbar';
import * as _ from 'lodash';

import { history } from '../components/App';
import store, { actions } from '../store';
import { ICard, IGame } from '../models';
import API from '../api';

const { dispatch, getState } = store;

export async function createGame(playersCount: number) {
    const res = await API.games.durak.createGame(playersCount)

    if(res && res.success) {
        const { games: { gameType, gameTypes } } = getState()

        dispatch(actions.game.createGame(res.game))

        history.push(
            `/${
                _.chain(gameTypes)
                    .find(({ id }) => id === gameType)
                    .get('name')
                    .lowerCase()
                    .value()
            }/${res.game.id}`
        )
    } else {
        snackbar.show(_.get(res, 'error'))
    }
}

export async function selectExistingGame(game: IGame): Promise<IGame | null> {
    const res = await API.games.durak.selectExistingGame(game.id)

    if(res && res.success) {
        dispatch(actions.game.getGame(res.game))

        return res.game
    } else {
        snackbar.show(_.get(res, 'error'))
    }

    return null
}

export async function attack(game: IGame, card: ICard): Promise<IGame | null> {
    const res = await API.games.durak.attack(game.id, card.id)

    if(res && res.success) {
        dispatch(actions.game.getGame(res.game))

        return res.game
    } else {
        snackbar.show(_.get(res, 'error'))
    }

    return null
}

export async function defence(game: IGame, cardForDefence: ICard, cardToAttack: ICard): Promise<IGame | null> {
    const res = await API.games.durak.defence(game.id, cardForDefence.id, cardToAttack.id)

    if(res && res.success) {
        dispatch(actions.game.getGame(res.game))

        return res.game
    } else {
        snackbar.show(_.get(res, 'error'))
    }

    return null
}