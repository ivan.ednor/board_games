import { IGame, IGameType } from '../models';
import store, { actions } from '../store';
import API from '../api';

const { dispatch } = store;

export async function getGameTypes(): Promise<IGameType[] | null> {
    const res = await API.games.getGameTypes()

    if(res && res.success) {
        dispatch(actions.game.getGameTypes(res.items))

        return res.items
    }

    return null
}

export async function getGame(gameId: number): Promise<IGame | null> {
    const res = await API.games.getGame(gameId)

    if(res && res.success) {
        dispatch(actions.game.getGame(res.game))

        return res.game
    }

    return null
}

export async function getExistingGames(gameType: number): Promise<IGame[] | null> {
    const res = await API.games.getExistingGames(gameType)

    if(res && res.success) {
        dispatch(actions.game.getExistingGames(res.items))

        return res.items
    }

    return null
}