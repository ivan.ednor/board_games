import * as durak from './durak';
import * as games from './games';
import * as user from './user';

export default {
    games: {
        ...games,
        durak
    },
    user
}