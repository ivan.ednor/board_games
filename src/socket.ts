import * as _ from 'lodash';

import { socketClients } from './main';
import helpers from './helpers';

export async function sendGame(gameModel: any): Promise<void> {
    _.forEach(socketClients, (client) => {
        const user = _.get(client.request.session, ['passport', 'user'])

        if(user) {
            client.emit(
                'game',
                helpers.durak.hideInfoInGame(
                    gameModel,
                    user
                )
            )
        }
    })
}