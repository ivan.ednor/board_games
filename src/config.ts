export const PORT = process.env.PORT || 1221;
export const SECRET: string = process.env.SECRET || 'session-secret';