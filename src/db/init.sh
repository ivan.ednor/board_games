#!/bin/sh

if [ $ENV = PROD ] || [ $ENV = DEV ]
then
   DB_NAME=board_games
else
   DB_NAME=board_games_test
fi

if [ $ENV = PROD ]
then
   PATH_TO_FILE=./dist/db/init.sql
else
   PATH_TO_FILE=./src/db/init.sql
fi

# Create database if not exist
psql $DB_USER $DB_PASSWORD -tc "SELECT 1 FROM pg_database WHERE datname = '$DB_NAME'" | grep -q 1 \
   || psql $DB_USER $DB_PASSWORD -c "CREATE DATABASE $DB_NAME"
psql $DB_USER -d $DB_NAME -f $PATH_TO_FILE