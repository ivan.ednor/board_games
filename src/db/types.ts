export interface IUser {
    id: string;
    email: string;
    password: string;
}

export interface IGameType {
    id: number;
    name: string;
}

export interface IGameMember {
    id: number;
    game_id: number;
    user_id: number;
    order: number;
}

export interface IGameCard {
    id: number;
    name: string;
    game_id: number;
    owner_id?: number;
    attacking_card_id?: number;
    status: CardStatus;
    img: string;
}

export interface IGame {
    id: number;
    type: number;
    turn_owner?: number;
    game_cards?: IGameCard[];
    game_members?: IGameMember[];
    players_count: number;
    status: GameStatus;
}

export enum CardStatus {
    IN_DECK = 'IN_DECK',
    IN_HAND = 'IN_HAND',
    IN_ATTACK = 'IN_ATTACK',
    IN_DEFENCE = 'IN_DEFENCE',
    IN_DISCARD_PILE = 'IN_DISCARD_PILE'
}

export enum GameStatus {
    INIT = 'INIT',
    ATTACK = 'ATTACK',
    DEFENCE = 'DEFENCE',
    DONE = 'DONE'
}