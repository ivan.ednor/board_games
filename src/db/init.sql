CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW."updatedAt" = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- USERS
CREATE TABLE IF NOT EXISTS users (
    "id" SERIAL PRIMARY KEY,
    "email" TEXT NOT NULL UNIQUE,
    "password" TEXT NOT NULL,

    "createdAt" TIMESTAMP NOT NULL DEFAULT NOW(),
    "updatedAt" TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON users
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

-- GAME TYPES
CREATE TABLE IF NOT EXISTS game_types (
    "id" SERIAL PRIMARY KEY,
    "name" TEXT NOT NULL UNIQUE
);
INSERT INTO game_types (name)
VALUES
    ('Durak')
ON CONFLICT DO NOTHING;

-- GAMES
CREATE TABLE IF NOT EXISTS games (
    "id" SERIAL PRIMARY KEY,
    "type" INTEGER NOT NULL,
    "turn_owner" INTEGER,
    "players_count" INTEGER NOT NULL,
    "status" TEXT NOT NULL,

    "createdAt" TIMESTAMP NOT NULL DEFAULT NOW(),
    "updatedAt" TIMESTAMP NOT NULL DEFAULT NOW(),

    FOREIGN KEY (type) REFERENCES game_types (id),
    FOREIGN KEY (turn_owner) REFERENCES users (id)
);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON games
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

-- GAME MEMBERS
CREATE TABLE IF NOT EXISTS game_members (
    "id" SERIAL PRIMARY KEY,
    "game_id" INTEGER NOT NULL,
    "user_id" INTEGER NOT NULL,
    "order" INTEGER NOT NULL,

    FOREIGN KEY (game_id) REFERENCES games (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

-- GAME CARD
CREATE TABLE IF NOT EXISTS game_cards (
    "id" SERIAL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "game_id" INTEGER NOT NULL,
    "owner_id" INTEGER,
    "attacking_card_id" INTEGER,
    "status" TEXT NOT NULL,
    "img" TEXT NOT NULL,

    FOREIGN KEY (game_id) REFERENCES games (id) ON DELETE CASCADE,
    FOREIGN KEY (owner_id) REFERENCES users (id) ON DELETE CASCADE,
    FOREIGN KEY (attacking_card_id) REFERENCES game_cards (id)
);