import * as Sequelize from 'sequelize';
import * as shell from 'shelljs';
import { hash } from 'bcrypt';

import { SALT_ROUNDS, ENV } from '../consts';
import { Omit } from '../types';

import { IGame, IGameCard, IGameMember, IGameType, IUser } from './types';

export class DB {
    constructor() {
        const dbName: string = ENV === 'PROD' || ENV === 'DEV'?
            'board_games'
            : 'board_games_test'

        this.sequelize = new Sequelize.Sequelize(`postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${dbName}`, {
            dialect: 'postgres'
        });
        this.defineModels();
        shell.exec(`${__dirname}/init.sh`);
    };

    private defineModels = async () => {
        // User
        this.models.User = this.sequelize.define('user', {
            email: {
                type: Sequelize.STRING,
                unique: true,
                validate: {
                    isEmail: true
                }
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            }
        });

        this.models.User.beforeCreate(async (user) => {
            user.setDataValue(
                'password',
                await hash(
                    user.getDataValue('password'),
                    SALT_ROUNDS
                )
            );
        })

        // GameType
        this.models.GameType = this.sequelize.define('game_types', {
            name: {
                type: Sequelize.STRING,
                unique: true
            }
        }, { timestamps: false });

        // Game Member
        this.models.GameMember = this.sequelize.define('game_members', {
            game_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            order: {
                type: Sequelize.INTEGER,
                allowNull: false
            }
        }, { timestamps: false });

        // Game Card
        this.models.GameCard = this.sequelize.define('game_cards', {
            name: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            game_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            owner_id: {
                type: Sequelize.INTEGER
            },
            attacking_card_id: {
                type: Sequelize.INTEGER
            },
            status: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            img: {
                type: Sequelize.TEXT,
                allowNull: false
            }
        }, { timestamps: false });

        // Game
        this.models.Game = this.sequelize.define('games', {
            type: {
                type: Sequelize.INTEGER,
                unique: true
            },
            turn_owner: {
                type: Sequelize.INTEGER
            },
            players_count: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            status: {
                type: Sequelize.TEXT,
                allowNull: false
            }
        });

        // Relations
        this.models.Game.hasMany(this.models.GameCard, { foreignKey: 'game_id' })
        this.models.GameCard.belongsTo(this.models.Game, { foreignKey: 'game_id' })
        this.models.Game.hasMany(this.models.GameMember, { foreignKey: 'game_id' })
        this.models.GameMember.belongsTo(this.models.Game, { foreignKey: 'game_id' })
    };
    private sequelize: Sequelize.Sequelize;
    
    public models: {
        User: Sequelize.ModelCtor<Sequelize.Model<IUser, Omit<IUser, 'id'>>>,
        GameType: Sequelize.ModelCtor<Sequelize.Model<IGameType, Omit<IGameType, 'id'>>>,
        GameMember: Sequelize.ModelCtor<Sequelize.Model<IGameMember, Omit<IGameMember, 'id'>>>,
        GameCard: Sequelize.ModelCtor<Sequelize.Model<IGameCard, Omit<IGameCard, 'id'>>>,
        Game: Sequelize.ModelCtor<Sequelize.Model<IGame, Omit<IGame, 'id'>>>
    } = {
        User: null,
        GameType: null,
        Game: null,
        GameMember: null,
        GameCard: null
    };
}

export default new DB();