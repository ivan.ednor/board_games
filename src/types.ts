import { Request } from 'express';
import { DB } from './db';

export interface IAppRequest extends Request {
    db: DB
};

export interface ISocket {
    id: string,
    idToView: string,
    nickname?: string
};

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export interface ISize {
    width: number,
    height: number
};