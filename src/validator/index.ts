import * as _ from 'lodash';
import { IKeyType, TypeMap } from './types';
import { Response } from 'express';
import { failResponse } from '../routes/utils';

class Validator {
    validateBody(body: any, keys: IKeyType[], res: Response): boolean {
        let result: boolean = true;

        _.forEach(keys, ({ key, type, range }) => {
            if(!_.has(body, key)) {
                return result = false;
            }

            const value: any = body[key];

            switch(type) {
                case TypeMap.STRING: {
                    if(!_.isString(value)) {
                        return result = false;
                    }

                    break;
                }
                case TypeMap.NUMBER: {
                    if(!_.isNumber(value)) {
                        return result = false;
                    }

                    if(range) {
                        if(value < range[0] || value > range[1]) {
                            return result = false;
                        }
                    }

                    break;
                }
                case TypeMap.ARRAY: {
                    if(!_.isArray(value)) {
                        return result = false;
                    }

                    break;
                }
                case TypeMap.OBJECT: {
                    if(!_.isObject(value)) {
                        return result = false;
                    }

                    break;
                }
            }
        });

        if(!result) {
            failResponse(res, 'Body validation error!');
        }

        return result;
    }
}

export default new Validator();