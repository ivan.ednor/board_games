export enum TypeMap {
    STRING,
    NUMBER,
    OBJECT,
    ARRAY
}

export interface IKeyType {
    key: string;
    type: TypeMap;
    range?: [number, number];
}