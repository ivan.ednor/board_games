type IEqualsFunction<IItemType> = (item1: IItemType, item2: IItemType) => boolean;

declare module 'buckets-js' {
    export class LinkedList<IItemType> {
        add: (item: IItemType, index?: number) => boolean;
        clear: () => boolean;
        contains: (
            item: IItemType,
            equalsFunction: IEqualsFunction<IItemType>
        ) => boolean;
        elementAtIndex: (index: number) => IItemType;
        equals: (
            other: LinkedList<IItemType>,
            equalsFunction: IEqualsFunction<IItemType>
        ) => boolean;
        first: () => IItemType;
        forEach: (
            callback: (item: IItemType) => void
        ) => void;
        indexOf: (
            item: IItemType,
            equalsFunction: IEqualsFunction<IItemType>
        ) => number;
        isEmpty: () => boolean;
        last: () => IItemType;
        remove: (item: IItemType, equalsFunction?: IEqualsFunction<IItemType>) => boolean;
        removeElementAtIndex: (index: number) => IItemType;
        reverse: () => void;
        size: () => number;
        toArray: () => IItemType[]
    }
}