import * as _ from 'lodash';

import { CardStatus, IGame, IGameCard, IUser } from '../db/types';

export function hideInfoInGame(
    gameModel: any,
    user: IUser
): IGame {
    const game: IGame = _.get(gameModel, ['dataValues'])

    return {
        ...game,
        game_cards: _.map(game.game_cards, (card) => {
            const canUserViewCard = card.owner_id === _.toNumber(user.id)
                || card.status === CardStatus.IN_ATTACK
                || card.status === CardStatus.IN_DEFENCE
                || card.status === CardStatus.IN_DISCARD_PILE
            const cardClone = _.cloneDeep(card)

            if(!canUserViewCard) {
                cardClone.img = ''
                cardClone.name = ''
            }

            return cardClone
        })
    }
}

export function checkIfCanAttackAgain(
    inTurnField: IGameCard[],
    inHand: IGameCard[]
): boolean {
    function removeCardSuit(cardName: string): string {
        return _.chain(cardName)
            .replace('_C', '')
            .replace('_D', '')
            .replace('_H', '')
            .replace('_S', '')
            .value()
    }

    return _.some(inHand, ({ name }) =>
        _.includes(
            _.map(inTurnField, ({ name }) => removeCardSuit(name)),
            removeCardSuit(name)
        )
    )
}