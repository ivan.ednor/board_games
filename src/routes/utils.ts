import * as _ from 'lodash';
import { Response } from 'express';
import { IServerResponse } from './types';

export function successResponse(res: Response, responseData: any): void {
    res.json({
        success: true,
        ...responseData
    } as IServerResponse);
}

export function failResponse(res: Response, error: string): void {
    res.json({
        success: false,
        error: _.toString(error)
    } as IServerResponse);
}