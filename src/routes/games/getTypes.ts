import { Request, Response } from 'express';
import * as _ from 'lodash';

import { failResponse, successResponse } from '../utils';
import db from '../../db';

export async function getTypes(req: Request, res: Response): Promise<void> {
    try {
        const gameTypes = await db.models.GameType.findAll()
    
        successResponse(res, { items: gameTypes })
    } catch(err) {
        failResponse(res, err)
    }
}