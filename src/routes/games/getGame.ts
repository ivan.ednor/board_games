import { Request, Response } from 'express';
import * as _ from 'lodash';

import { failResponse, successResponse } from '../utils';
import helpers from '../../helpers';
import db from '../../db';

export async function getGame(req: Request, res: Response): Promise<void> {
    try {
        successResponse(
            res,
            {
                game: helpers.durak.hideInfoInGame(
                    await db.models.Game.findOne({
                        where: {
                            id: req.params.id
                        },
                        include: [db.models.GameCard, db.models.GameMember]
                    }),
                    req.user
                )
            }
        )
    } catch(err) {
        failResponse(res, err)
    }
}