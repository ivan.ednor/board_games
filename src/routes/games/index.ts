import { Router } from 'express';

import { getExistingGames } from './getExistingGames';
import { getTypes } from './getTypes';
import { getGame } from './getGame';
import durakRouter from './durak';

const router = Router();

router.use('/durak', durakRouter);

router.get('/types', getTypes)

router.get('/:id', getGame)

router.get('/types/:id', getExistingGames)

export default router;