import { Request, Response } from 'express';
import * as _ from 'lodash';

import { failResponse, successResponse } from '../utils';
import helpers from '../../helpers';
import db from '../../db';

export async function getExistingGames(req: Request, res: Response): Promise<void> {
    try {
        const games = await db.models.Game.findAll({
            where: {
                type: req.params.id
            }
        })
    
        successResponse(
            res,
            {
                items: _.map(games, (game) =>
                    helpers.durak.hideInfoInGame(
                       game,
                       req.user 
                    )
                )
            }
        )
    } catch(err) {
        failResponse(res, err)
    }
}