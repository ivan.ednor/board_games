import { Request } from 'express';

export interface ICreateGame extends Request {
    body: {
        playersCount: number
    }
}