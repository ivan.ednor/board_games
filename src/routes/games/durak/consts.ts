import * as _ from 'lodash';

import { CardStatus, IGameCard } from '../../../db/types'

type CardType = Pick<IGameCard, 'name' | 'status' | 'img'>

function cardGenerator(nameBase: string, imgBase: string): CardType[] {
    return [
        {
            name: `${nameBase}_C`,
            status: CardStatus.IN_DECK,
            img: `/img/cards/${imgBase}C.png`
        },
        {
            name: `${nameBase}_D`,
            status: CardStatus.IN_DECK,
            img: `/img/cards/${imgBase}D.png`
        },
        {
            name: `${nameBase}_H`,
            status: CardStatus.IN_DECK,
            img: `/img/cards/${imgBase}H.png`
        },
        {
            name: `${nameBase}_S`,
            status: CardStatus.IN_DECK,
            img: `/img/cards/${imgBase}S.png`
        }
    ]
}

const cardNames: Array<{ nameBase: string; imgBase: string }> = [
    { nameBase: 'SIX', imgBase: '6' },
    { nameBase: 'SEVEN', imgBase: '7' },
    { nameBase: 'EIGHT', imgBase: '8' },
    { nameBase: 'NINE', imgBase: '9' },
    { nameBase: 'TEN', imgBase: '10' },
    { nameBase: 'JECK', imgBase: 'J' },
    { nameBase: 'QUEEN', imgBase: 'Q' },
    { nameBase: 'KING', imgBase: 'K' },
    { nameBase: 'ACE', imgBase: 'A' }
]

export const cards: CardType[] = _.chain(cardNames)
    .map(({ imgBase, nameBase }) => cardGenerator(nameBase, imgBase))
    .flatten()
    .value()