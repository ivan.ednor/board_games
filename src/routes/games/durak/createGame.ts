import { Response } from 'express';
import * as _ from 'lodash';

import { IKeyType, TypeMap } from '../../../validator/types';
import { failResponse, successResponse } from '../../utils';
import { GameStatus } from '../../../db/types';
import validator from '../../../validator';
import db from '../../../db';

import { ICreateGame } from './types';
import { cards } from './consts';

const registerTypeKeys: IKeyType[] = [{
    key: 'playersCount',
    type: TypeMap.NUMBER,
    range: [2, 4]
}]

export async function createGame(req: ICreateGame, res: Response): Promise<void> {
    try {
        if(!validator.validateBody(req.body, registerTypeKeys, res)) {
            return null
        }

        const gameType = await db.models.GameType.findOne({
            where: {
                name: 'Durak'
            }
        });

        const newGame = await db.models.Game.create({
            type: gameType.getDataValue('id'),
            players_count: req.body.playersCount,
            status: GameStatus.INIT
        })

        await db.models.GameMember.create({
            game_id: newGame.getDataValue('id'),
            user_id: req.user.id,
            order: 0
        })
        await db.models.GameCard.bulkCreate(
            _.map(cards, ({ img, status, name }) => ({
                game_id: newGame.getDataValue('id'),
                img,
                name,
                status
            }))
        )

        successResponse(
            res,
            {
                game: await db.models.Game.findOne({
                    where: {
                        id: newGame.getDataValue('id')
                    },
                    include: [db.models.GameCard, db.models.GameMember]
                })
            }
        )
    } catch(err) {
        failResponse(res, err)
    }
}