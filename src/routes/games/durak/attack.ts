import { Request, Response } from 'express';
import * as _ from 'lodash';

import { failResponse, successResponse } from '../../utils';
import sideEffects from '../../../sideEffects';
import db from '../../../db';

export async function attack(req: Request, res: Response): Promise<void> {
    try {    
        const game = await db.models.Game.findOne({
            where: {
                id: req.params.gameId
            },
            include: [db.models.GameCard, db.models.GameMember]
        })

        successResponse(
            res,
            {
                game: await sideEffects.durak.attack(
                    req,
                    _.get(game, ['dataValues'])
                )
            }
        )
    } catch(err) {
        failResponse(res, err)
    }
}