import { Request, Response } from 'express';
import * as _ from 'lodash';

import { failResponse, successResponse } from '../../utils';
import effects from '../../../sideEffects';
import db from '../../../db';

export async function selectExistingGame(req: Request, res: Response): Promise<void> {
    try {
        const game = await db.models.Game.findOne({
            where: {
                id: req.params.id
            }
        })
    
        successResponse(
            res,
            {
                game: await effects.durak.selectGame(
                    req,
                    _.get(game, ['dataValues'])
                )
            }
        )
    } catch(err) {
        failResponse(res, err)
    }
}