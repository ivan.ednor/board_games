import { Router } from 'express';

import { selectExistingGame } from './selectExistingGame';
import { createGame } from './createGame';
import { defence } from './defence';
import { attack } from './attack';

const router = Router();

router.post('/', createGame)

router.put('/:id', selectExistingGame)

router.post('/:gameId/attack/:cardId', attack)

router.post('/:gameId/defence/:cardForDefenceId/attack/:cardToAttackId', defence)

export default router;