import { Router } from 'express';

import gamesRouter from './games'
import authRouter from './auth';

const router: Router = Router();

router.use('/auth', authRouter);

router.use('/games', gamesRouter)

export default router;