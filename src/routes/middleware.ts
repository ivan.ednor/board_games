import { NextFunction, Request, Response } from 'express';
import { failResponse } from './utils';

export function isLogged(req: Request, res: Response, next: NextFunction): void {
    if(req.isAuthenticated()) {
        next();
    } else {
        failResponse(res, 'Not authorized');
    }
}