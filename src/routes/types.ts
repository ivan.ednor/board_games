export interface IServerResponse {
    success: boolean,
    error?: string
}