import * as _ from 'lodash'
import { compare } from 'bcrypt';
import { Done } from './types';
import db from '../../db';

export async function login(email: string, password: string, done: Done): Promise<boolean> {
    try {
        const user = await db.models.User.findOne({
            where: {
                email
            }
        });

        if(!user) {
            done(null, false);

            return false;
        }

        const isCorrectPassword: boolean = await compare(password, user.getDataValue('password'));

        if(!isCorrectPassword) {
            done(null, false);

            return false;
        }

        done(null, _.get(user, ['dataValues']));

        return true;
    } catch(err) {
        done(err);

        return false;
    }
}