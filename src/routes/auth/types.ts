import { Request } from 'express';
import { IVerifyOptions } from 'passport-local';
import { IUser } from '../../db/types';

export interface IRegister extends Request {
    body: {
        email: string,
        password: string
    }
}

export type Done = (error: any, user?: IUser | boolean, options?: IVerifyOptions) => void;