import { Router, Response, Request } from 'express';
import * as passport from 'passport';
import { Strategy } from 'passport-local';

import { successResponse, failResponse } from '../utils';
import { register } from './register';
import { login } from './login';
import { IUser } from '../../db/types';
import { isLogged } from '../middleware';

import { Done } from './types';

const router = Router();

passport.use(new Strategy({
    usernameField: 'email',
    passwordField: 'password',
}, login));
passport.serializeUser((user: IUser, done: Done) => {
    done(null, {
        id: user.id,
        email: user.email
    } as IUser);
});
passport.deserializeUser((user: IUser, done: Done) => {
    done(null, user);
});

router.post('/sign-up', register(true));
router.post('/login', passport.authenticate('local'), (req: Request, res: Response) => {
    successResponse(res, {});
});
router.get('/me', (req: Request, res: Response) => {
    if(req.user) {
        successResponse(res, {
            user: req.user
        });
    } else {
        failResponse(res, 'Not autorized');
    }
});
router.delete('/logout', (req: Request, res: Response) => {
    try {
        req.logout();
        successResponse(res, {});
    } catch(err) {
        failResponse(res, err);
    }
});
router.post('/check-email-availability', register(false))
router.get('/get-me', isLogged, (req: Request, res: Response) => {
    successResponse(res, {
        user: req.user
    })
})

export default router;