import { Response } from 'express';

import validator from '../../validator';
import { TypeMap, IKeyType } from '../../validator/types';
import db from '../../db';
import { successResponse, failResponse } from '../utils';

import { IRegister } from './types';

export function register(doCreateUser: boolean) {
    const registerTypeKeys: IKeyType[] = [{
        key: 'email',
        type: TypeMap.STRING
    }];

    if(doCreateUser) {
        registerTypeKeys.push({
            key: 'password',
            type: TypeMap.STRING
        })
    }

    return async function (req: IRegister, res: Response): Promise<void> {
        try {
            if(!validator.validateBody(req.body, registerTypeKeys, res)) {
                return null;
            }

            const { email, password } = req.body;
            const userWithSameEmail = await db.models.User.findOne({
                where: {
                    email
                }
            });

            if(userWithSameEmail) {
                throw 'User with this email already exists!';
            }

            if(doCreateUser) {
                await db.models.User.create({
                    email,
                    password
                })
            }

            successResponse(res, {});
        } catch(err) {
            failResponse(res, err);
        }
    }
}