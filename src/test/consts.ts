import * as _ from 'lodash';
import { IUser } from '../db/types';

export const user: IUser = {
    email: 'name@mail.ru',
    id: _.uniqueId(),
    password: 'password'
};