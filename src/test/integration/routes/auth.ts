require('dotenv').config()

import { expect } from 'chai';
import { Response } from 'express';
import { IServerResponse } from '../../../routes/types';
import { register } from '../../../routes/auth/register';
import { login } from '../../../routes/auth/login';
import { IRegister } from '../../../routes/auth/types';
import db from '../../../db';

describe('auth routes test', () => {
    const clearDB = async () => {
        await db.models.User.destroy({
            where: {
                email: 'test@mail.ru'
            }
        });
    };

    before(clearDB);

    it('register', async () => {
        const registerResponse: IServerResponse = await new Promise<IServerResponse>((resolve) => {
            register(true)({
                body: {
                    email: 'test@mail.ru',
                    password: 'password'
                }
            } as IRegister, {
                json: (obj) => {
                    resolve(obj);
                }
            } as Response);
        });

        expect(registerResponse.success).to.be.true;
    });

    it('login', async () => {
        const loginRes: boolean = await login('test@mail.ru', 'password', () => {});

        expect(loginRes).to.be.true;
    });

    after(clearDB);
});