import * as selectGame from './selectGame';
import * as defence from './defence';
import * as attack from './attack';

export default {
    ...selectGame,
    ...attack,
    ...defence
}