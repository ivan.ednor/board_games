import { Request } from 'express';
import * as _ from 'lodash';

import { CardStatus, GameStatus, IGame } from '../../db/types';
import { sendGame } from '../../socket';
import helpers from '../../helpers';
import db from '../../db';

export async function attack(req: Request, game: IGame): Promise<IGame> {
    await db.models.GameCard.update(
        {
            status: CardStatus.IN_ATTACK
        },
        {
            where: {
                id: req.params.cardId,
                game_id: req.params.gameId,
                status: CardStatus.IN_HAND,
                owner_id: req.user.id
            }
        }
    )

    const cardsInTurnField = await db.models.GameCard.findAll({
        where: {
            status: [CardStatus.IN_ATTACK, CardStatus.IN_DEFENCE],
            game_id: req.params.gameId
        }
    })
    const cardsInPlayersHand = await db.models.GameCard.findAll({
        where: {
            status: CardStatus.IN_HAND,
            game_id: req.params.gameId,
            owner_id: req.user.id
        }
    })

    const canAttackAgain = helpers.durak.checkIfCanAttackAgain(
        _.map(cardsInTurnField, 'dataValues'),
        _.map(cardsInPlayersHand, 'dataValues')
    )

    if(!canAttackAgain) {
        const currentGameMember = await db.models.GameMember.findOne({
            where: {
                game_id: req.params.gameId,
                user_id: req.user.id
            }
        })

        const nextGameMember = await db.models.GameMember.findOne({
            where: {
                game_id: req.params.gameId,
                order: (game.players_count - 1) === currentGameMember.getDataValue('order')? 0: currentGameMember.getDataValue('order') + 1
            }
        })

        await db.models.Game.update(
            {
                turn_owner: nextGameMember.getDataValue('user_id'),
                status: GameStatus.DEFENCE
            },
            {
                where: {
                    id: req.params.gameId
                }
            }
        )
    }

    const res = await db.models.Game.findOne({
        where: {
            id: game.id
        },
        include: [db.models.GameCard, db.models.GameMember]
    })

    await sendGame(res)

    return helpers.durak.hideInfoInGame(
        res,
        req.user
    )
}