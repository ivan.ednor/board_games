import { Request } from 'express'
import * as _ from 'lodash';

import { CardStatus, GameStatus, IGame, IGameMember } from '../../db/types'
import { sendGame } from '../../socket';
import helpers from '../../helpers';
import db from '../../db'

export async function selectGame(req: Request, game: IGame): Promise<IGame> {
    const gameMembers = await db.models.GameMember.findAll({
        where: {
            game_id: req.params.id
        },
        order: [
            ['order', 'ASC']
        ]
    })

    if(_.every(gameMembers, (member) => member.getDataValue('user_id') !== req.user.id)) {
        gameMembers.push(
            await db.models.GameMember.create({
                game_id: _.toNumber(req.params.id),
                user_id: req.user.id,
                order: _.chain(gameMembers)
                    .map((member) => _.get(member, ['dataValues', 'order']))
                    .max()
                    .value() + 1
            })
        )
    }

    if(gameMembers.length === game.players_count && game.status === GameStatus.INIT) {
        const firstPlayer: IGameMember = _.chain(gameMembers)
            .map('dataValues')
            .minBy('order')
            .value()

        await db.models.Game.update(
            {
                turn_owner: firstPlayer.user_id,
                status: GameStatus.ATTACK
            },
            {
                where: {
                    id: game.id
                }
            }
        )

        for(let i = 0; i < gameMembers.length; i++) {
            const player = _.get(gameMembers, [i, 'dataValues'])
            const cardsToMoveInHand = await db.models.GameCard.findAll({
                where: {
                    game_id: game.id,
                    status: CardStatus.IN_DECK
                },
                limit: 6
            })
            await db.models.GameCard.update(
                {
                    status: CardStatus.IN_HAND,
                    owner_id: player.user_id
                },
                {
                    where: {
                        id: _.map(cardsToMoveInHand, (card) => card.getDataValue('id'))
                    }
                }
            )
        }
    }

    const res = await db.models.Game.findOne({
        where: {
            id: game.id
        },
        include: [db.models.GameCard, db.models.GameMember]
    })

    await sendGame(res)

    return helpers.durak.hideInfoInGame(
        res,
        req.user
    )
}