import { Request } from 'express';
import * as _ from 'lodash';

import { CardStatus, GameStatus, IGame, IGameCard } from '../../db/types';
import { sendGame } from '../../socket';
import helpers from '../../helpers';
import db from '../../db';

export async function defence(req: Request, game: IGame): Promise<IGame> {
    await db.models.GameCard.update(
        {
            status: CardStatus.IN_DEFENCE,
            attacking_card_id: _.toNumber(req.params.cardToAttackId)
        },
        {
            where: {
                id: req.params.cardForDefenceId,
                game_id: req.params.gameId,
                status: CardStatus.IN_HAND,
                owner_id: req.user.id
            }
        }
    )

    let cardsInTurnField: IGameCard[] = _.map(
        await db.models.GameCard.findAll({
            where: {
                status: [CardStatus.IN_ATTACK, CardStatus.IN_DEFENCE],
                game_id: req.params.gameId
            }
        }),
        'dataValues'
    ) as any
    _.chain(cardsInTurnField)
        .filter((card) => card.status === CardStatus.IN_ATTACK)
        .forEach((card) => {
            const defendingCard = _.find(
                cardsInTurnField,
                (item) => item.attacking_card_id === card.id
            )

            if(!!defendingCard) {
                cardsInTurnField = _.reject(cardsInTurnField, ({ id }) => id === card.id)
            }
        }).value()

    if(_.every(cardsInTurnField, ({ status }) => status === CardStatus.IN_DEFENCE)) {
        await db.models.GameCard.update(
            {
                attacking_card_id: null,
                owner_id: null,
                status: CardStatus.IN_DISCARD_PILE
            },
            {
                where: {
                    game_id: req.params.gameId,
                    status: [CardStatus.IN_ATTACK, CardStatus.IN_DEFENCE]
                }
            }
        )

        for(let i = 0; i < game.game_members.length; i++) {
            const member = game.game_members[i]
            const cards = await db.models.GameCard.findAll({
                where: {
                    game_id: req.params.gameId,
                    owner_id: member.user_id,
                    status: CardStatus.IN_HAND
                }
            })

            if(cards.length < 6) {
                const cardsToMoveInHand = await db.models.GameCard.findAll({
                    where: {
                        game_id: req.params.gameId,
                        status: CardStatus.IN_DECK
                    },
                    limit: 6 - cards.length
                })
                
                await db.models.GameCard.update(
                    {
                        status: CardStatus.IN_HAND,
                        owner_id: member.user_id
                    },
                    {
                        where: {
                            id: _.map(cardsToMoveInHand, (card) => card.getDataValue('id'))
                        }
                    }
                )
            }
        }

        await db.models.Game.update(
            {
                status: GameStatus.ATTACK,
                turn_owner: req.user.id
            },
            {
                where: {
                    id: req.params.gameId
                }
            }
        )
    }

    const res = await db.models.Game.findOne({
        where: {
            id: game.id
        },
        include: [db.models.GameCard, db.models.GameMember]
    })

    await sendGame(res)

    return helpers.durak.hideInfoInGame(
        res,
        req.user
    )
}