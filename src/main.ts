require('dotenv').config()

import * as expressSession from 'express-session';
import * as bodyParser from 'body-parser';
import * as passport from 'passport';
import * as express from 'express';
import * as moment from 'moment';
import * as morgan from 'morgan';
import * as socketIO from 'socket.io';
import * as path from 'path';
import * as _ from 'lodash';

import router from './routes';

import { PORT, SECRET } from './config';

const sessionMiddleware: express.RequestHandler = expressSession({
    resave: true,
    secret: SECRET,
    saveUninitialized: false
});
const app: express.Express = express();

app.use(sessionMiddleware)
    .use(bodyParser.json())
    .use(passport.initialize())
    .use(passport.session())
    .use(morgan((tokens, req, res) => [
        `[${moment().format('HH:mm:ss')}]`,
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'),
        '-',
        tokens['response-time'](req, res), 'ms'
    ].join(' '))).use('/api', router)
    .use(express.static(path.join(__dirname, '../client/build')))
    .use((req, res) => {
        res.sendFile(path.join(
            __dirname + '/../client/build/index.html'
        ));
    });

app.listen(PORT);
console.log(`Server started on port ${PORT}`);

// Socket
export const socketClients: any[] = []

const socket = new socketIO.Server()

socket.use(({ request }, next) => {
    // @ts-ignore
    sessionMiddleware(request, request.res || {}, next);
});

socket.on('connection', (client) => {
    socketClients.push(client)
})

socket.listen(_.toNumber(PORT) + 1);